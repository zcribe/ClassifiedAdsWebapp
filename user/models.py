from django.db import models
from django.contrib.auth.models import User
from mimesis import Person


def _generate_fake_user(locale='et'):
    g = Person(locale)
    fake = User()
    fake.username = g.username()
    fake.first_name = g.surname()
    fake.last_name = g.last_name()
    fake.email = g.email()
    fake.save()
    return User.objects.get(username=fake.username)

