from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import DetailView


class ProfileView(LoginRequiredMixin, DetailView):
    template_name = 'profile.html'
    model = User

