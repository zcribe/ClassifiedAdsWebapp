from django.test import LiveServerTestCase
from selenium import webdriver


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_can_choose_ad_category(self):
        # Joe is looking to buy a new bike. He visits a site and checks out its homepage.
        self.browser.get(self.live_server_url)

        # He notices the page title and header that tell him that this is an ads page.
        self.assertIn('Classified Ads', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Classified Ads', header_text)

        # He notices page filled with categories of ads and is invited to pick one.
        list_exists = self.browser.find_elements_by_tag_name('ul')
        list_line_exists = self.browser.find_elements_by_tag_name('li')

        self.assertTrue(list_exists)
        self.assertTrue(list_line_exists)

        # He is looking for a bike so he picks bikes category.
        self.assertIn('Bikes', [line.text for line in list_line_exists])


        # He picks one and clicks on it and it forwards him to ads page.
        self.fail('Finish test!')

