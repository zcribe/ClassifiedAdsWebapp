from django.db import models
from autoslug import AutoSlugField
from django.contrib.auth.models import User
from datetime import datetime, timedelta


from taggit.managers import TaggableManager
import django_filters


class Category(models.Model):
    name = models.CharField(default='', max_length=300)
    slug = AutoSlugField(populate_from='name')
    icon = models.ImageField(upload_to='images/icons', default='placeholder.jpg')

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField(default='', max_length=300)
    slug = AutoSlugField(populate_from='name')
    icon = models.ImageField(upload_to='images/icons', default='placeholder.jpg')
    image = models.ImageField(upload_to='images', default='placeholder.jpg')
    category = models.ForeignKey('Category',
                                 on_delete=models.CASCADE,)

    def __str__(self):
        return self.name


class ClassifiedAd(models.Model):
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(User,
                                on_delete=models.CASCADE,
                                default=0)
    name = models.CharField(default='', max_length=300)
    slug = AutoSlugField(populate_from='name')
    transaction_type = models.CharField(default='unspecified', max_length=20)
    price = models.IntegerField(default=0)
    description = models.TextField(default='')
    views = models.IntegerField(default=0)
    lifetime = models.IntegerField(default=14)
    subcategory = models.ForeignKey('SubCategory',
                                    on_delete=models.CASCADE,)
    tags = TaggableManager()

    def __str__(self):
        return self.name

    @property
    def is_expired(self):
        if datetime.now() >= (self.created + timedelta(days=self.lifetime)):
            return True
        return False

    @property
    def will_expire(self):
        return self.created + timedelta(days=self.lifetime)

    @staticmethod
    def _bootstrap(locale='et'):
        from mimesis import Generic
        from user.models import _generate_fake_user
        import random
        g = Generic(locale)

        fail_image = None
        fail_image_second = None

        for _ in range(0, 22):
            cat = Category()
            cat.name = g.text.word().title()
            cat.icon = random.choice(['fa-ad', 'fa-accessible-icon', 'fa-accusoft', 'fa-adn', 'fa-american-sign-language-interpreting', 'fa-apple-alt'])
            cat.save()
            for _ in range(0, random.randint(2, 10)):
                sub = SubCategory()
                sub.name = g.text.word().title()
                sub.category = cat
                sub.icon = random.choice(['fa-ad', 'fa-accessible-icon', 'fa-accusoft', 'fa-adn', 'fa-american-sign-language-interpreting', 'fa-apple-alt'])
                try:
                    sub.image = g.internet.stock_image(200, 200, ['buy', 'sell', 'rent'])
                    fail_image = sub.image
                except:
                    sub.image = fail_image
                sub.save()
                for _ in range(0, random.randint(10, 50)):
                    ad = ClassifiedAd()
                    ad.subcategory = sub
                    ad.name = g.text.word().title()
                    ad.creator = _generate_fake_user()
                    ad.description = g.text.text()
                    ad.transaction_type = random.choice(['Ost', 'Müük', 'Rent', 'Tutvus'])
                    ad.lifetime = g.numbers.between(3, 35)
                    ad.price = g.numbers.between(1, 500000)
                    ad.views = g.numbers.between(1, 7000)
                    ad.save()
                    for _ in range(0, random.randint(2, 4)):
                        images = ClassifiedAdImage()
                        images.associated_ad = ad
                        try:
                            images.image = g.internet.stock_image(600, 600)
                            fail_image_second = images.image
                        except:
                            images.image = fail_image_second

                        images.save()


class ClassifiedAdImage(models.Model):
    image = models.ImageField(default='placeholder.jpg')
    associated_ad = models.ForeignKey('ClassifiedAd',
                                      on_delete=models.CASCADE,
                                      related_name='images')


class ClassifiedAdFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iexact')

    class Meta:
        model = ClassifiedAd
        fields = {'name': [],
                  'created': [],
                  'transaction_type': [],
                  'price': ['lt', 'gt']
                  }
