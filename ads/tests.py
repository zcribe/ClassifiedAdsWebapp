from django.test import TestCase
from ads.models import Category, SubCategory, ClassifiedAd
from mimesis import Generic


class GenerateTestData:
    def setup_fake_category(self):
        first_category = Category()
        first_category.name = 'SampleCategory1'
        first_category.save()
        return first_category

    def setup_fake_subcategory(self):
        fake_category = self.setup_fake_category()
        first_subcategory = SubCategory()
        first_subcategory.name = 'SampleSubCategory1'
        first_subcategory.category = fake_category
        first_subcategory.save()
        return first_subcategory

    def setup_fake_ad(self):
        fake_subcategory = self.setup_fake_subcategory()
        first_ad = ClassifiedAd()
        first_ad.name = 'SampleSubCategory1'
        first_ad.subcategory = fake_subcategory
        first_ad.save()
        return first_ad


class ModelTest(TestCase):

    def test_saving_and_retrieving_category(self):
        category = GenerateTestData().setup_fake_category()
        saved_categories = Category.objects.all()
        self.assertEqual(saved_categories.count(), 1)

        first_saved_category = saved_categories[0]
        self.assertEqual(first_saved_category.name, 'SampleCategory1')

    def test_saving_and_retrieving_subcategory(self):
        category = GenerateTestData().setup_fake_category()


        saved_subcategories = SubCategory.objects.all()
        self.assertEqual(saved_subcategories.count(), 1)

        first_saved_subcategory = saved_subcategories[0]
        self.assertEqual(first_saved_subcategory.name, 'SampleSubCategory1')

    def test_saving_and_retrieving_classified_ad(self):
        subcategory = GenerateTestData().setup_fake_subcategory()

        first_classifiedad = ClassifiedAd()
        first_classifiedad.name = 'SampleClassifiedAd1'
        first_classifiedad.subcategory = subcategory
        first_classifiedad.save()

        saved_classifiedad = ClassifiedAd.objects.all()
        self.assertEqual(saved_classifiedad.count(), 1)

        first_saved_subcategory = saved_classifiedad[0]
        self.assertEqual(first_saved_subcategory.name, 'SampleClassifiedAd1')


class HomePageTest(TestCase):

    def test_uses_home_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'home.html')


class SubCategoryPageTest(TestCase):

    def test_uses_subcategory_template(self):
        g = GenerateTestData()
        fake_category = g.setup_fake_category()
        fake_subcategory = g.setup_fake_subcategory(fake_category)
        response = self.client.get(f'{fake_category}/{fake_subcategory}')
        self.assertTemplateUsed(response, 'subcategory.html')


class ClassifiedAdPageTest(TestCase):

    def test_uses_subcategory_template(self):
        g = GenerateTestData()
        fake_category = g.setup_fake_category()
        fake_subcategory = g.setup_fake_subcategory(fake_category)
        fake_ad = g.setup_fake_ad(fake_subcategory)
        response = self.client.get(f'{fake_category}/{fake_subcategory}/{fake_ad}')
        self.assertTemplateUsed(response, 'classifiedad.html')



