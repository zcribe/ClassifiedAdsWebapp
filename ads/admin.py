from django.contrib import admin
from ads.models import Category, SubCategory, ClassifiedAd, ClassifiedAdImage

# Register your models here.
admin.site.register(Category)
admin.site.register(SubCategory)


class ClassifiedAdImageInline(admin.TabularInline):
    model = ClassifiedAdImage
    extra = 3


class ClassifiedAdAdmin(admin.ModelAdmin):
    inlines = [ClassifiedAdImageInline, ]


admin.site.register(ClassifiedAd, ClassifiedAdAdmin)
