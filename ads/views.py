from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from ads.models import Category, SubCategory, ClassifiedAd
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class HomePageView(ListView):
    template_name = "ads/home.html"
    model = Category


class SubCategoryView(DetailView):
    template_name = "ads/subcategory.html"
    model = SubCategory


class ClassifiedAdView(DetailView):
    template_name = "ads/classifiedad.html"
    model = ClassifiedAd


class ClassifiedAdViewNew(CreateView, LoginRequiredMixin):
    template_name = "ads/classifiedad_add.html"
    model = ClassifiedAd
    fields = ['name', 'transaction_type', 'price', 'description', 'subcategory', ]


class ClassifiedAdViewDelete(DeleteView, LoginRequiredMixin):
    template_name = "ads/classifiedad_delete.html"
    model = ClassifiedAd
    fields = ['name', 'transaction_type', 'price', 'description', 'subcategory', ]


class ClassifiedAdViewUpdate(UpdateView, LoginRequiredMixin):
    template_name = "ads/classifiedad_update.html"
    model = ClassifiedAd
    fields = ['name', 'transaction_type', 'price', 'description', 'subcategory', ]

