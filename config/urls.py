"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.conf import settings
from django.contrib import admin
from django.urls import include
from django.urls import path
from django.conf.urls import url
from ads import views
from user import views as userviews

from django_distill import distill_path

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^accounts/', include('django_registration.backends.activation.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/profile/<int:pk>', userviews.ProfileView.as_view(), name='profile'),
    path('', views.HomePageView.as_view(), name='home'),
    path('<str:category>/<str:slug>', view=views.SubCategoryView.as_view(), name='subcategory'),
    path('<str:category>/<str:subcategory>/<int:pk>', view=views.ClassifiedAdView.as_view(), name='classifiedad'),
    path('new/', view=views.ClassifiedAdViewNew.as_view(success_url='classifiedad_new_success'), name='newad'),
    path('<str:category>/<str:subcategory>/<int:pk>/delete', view=views.ClassifiedAdViewDelete.as_view(), name='deletead'),
    path('<str:category>/<str:subcategory>/<int:pk>/update', view=views.ClassifiedAdViewUpdate.as_view(success_url='classifiedad_update_success'), name='updatead'),
]

