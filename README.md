# Classified ads website

A fairly simple classified ads website. It provides the core simple features.


* Ads CRUD
* Accounts
* Search 
* Hierarchical tree structure
* Optimised UX for this structure
* Image accommodating clean design

# Tech
* Python
* Django
* Bootstrap
* jQuery



## Screenshots
![Index](https://github.com/zcribe/ClassifiedAdsWebapp/blob/master/docs/screenshots/1.png)
![Subcategory](https://github.com/zcribe/ClassifiedAdsWebapp/blob/master/docs/screenshots/2.png)
![Product](https://github.com/zcribe/ClassifiedAdsWebapp/blob/master/docs/screenshots/3.png)
![Login](https://github.com/zcribe/ClassifiedAdsWebapp/blob/master/docs/screenshots/4.png)
 